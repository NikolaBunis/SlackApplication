/*26.06.2018, Nikola Bunis

A message class for some basic functionality of Message objects that will interact with Channel objects.
Will also be a base class for four additional sub-classes representing types of message.
*/


public class Message {

    private int timestamp;
    private String author;

    public Message(int timestamp, String author) {
        setTimestamp(timestamp);
        setAuthor(author);
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }
}
