/*26.06.2018, Nikola Bunis

This enum will depict the icon types for objects of class IconMessage.

Initial concept:

Friendly, Funny, Neutral, Negative, Obscene, Other


 */

public enum IconType {

    FRIENDLY, FUNNY, NEUTRAL, NEGATIVE, OBSCENE, OTHER

}

