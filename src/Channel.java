/*26.08.2018, Nikola Bunis
Main channel class,

Should have the following attributes:

- name
- is it the main channel, a boolean variable for now
- list of message objects

Should have the following behaviours:

- create new (instance)
- switch current (via isCurrent variable)
- some message functionality that will be mainly in a separate class
- show history of messages
- clear said history
 */

import java.util.Map;

public class Channel {


    //some basic attributes
    private String name;

    private boolean isCurrent;


    private Map<Integer, Message> history;

    public Channel(String name, boolean isCurrent) {
        setName(name);
        setCurrent(isCurrent);
    }

    public Channel(String name) {
        setName(name);
    }



    public String getName() {
        return name;
    }

    //some validation would be good, but it's not set as a condition
    public void setName(String name) {
        this.name = name;
    }

    public boolean isCurrent() {
        return isCurrent;
    }

    public void setCurrent(boolean current) {
        isCurrent = current;
    }

    public Map<Integer, Message> getHistory() {
        return history;
    }

    public void setHistory(Map<Integer, Message> history) {
        getHistory().putAll(history);
    }

    public void addMessage(Message message){

        getHistory().put(message.getTimestamp(), message);

    }

    public void displayLikes(Likable message){

        System.out.println();




    }

    public void showHistory(Map<Integer, Message> history){

        System.out.println(history);

    }

    public void clearHistory(Map<Integer, Message> history){

        getHistory().clear();

    }


}
