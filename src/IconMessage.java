/*26.06.2018, Nikola Bunis

This class extends Message by having an icon name as a placeholder for the actual icon and a type in it.

Types will be outlined in a separate enum class

 */

public class IconMessage extends Message {


    private String iconName;

    private IconType iconType;


    public IconMessage(int timestamp, String author) {
        super(timestamp, author);
    }

    public IconMessage(int timestamp, String author, String iconName) {
        super(timestamp, author);
        setIconName(iconName);
    }

    public IconMessage(int timestamp, String author, String iconName, IconType iconType) {
        super(timestamp, author);
        setIconName(iconName);
        setIconType(iconType);
    }


    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }

    public IconType getIconType() {
        return iconType;
    }

    public void setIconType(IconType iconType) {
        this.iconType = iconType;
    }

}
