/*26.06.2018, Nikola Bunis

Sub-class of the Message class.

Will extend it with having a string filed for image name instead of an actual image(for now)
and a like counter for the like functionality planned in this project.

Should support the download functionality.


 */


public class ImageMessage extends Message {

    private String imageName;

    private int likeCounter;

    public ImageMessage(int timestamp, String author) {
        super(timestamp, author);
    }

    public ImageMessage(int timestamp, String author, String imageName) {
        super(timestamp, author);
        setImageName(imageName);
    }


    public String getImageName() {
        return imageName;
    }

    public void setImageName(String imageName) {
        this.imageName = imageName;
    }

    public int getLikeCounter() {
        return likeCounter;
    }

    public void setLikeCounter(int likeCounter) {
        this.likeCounter = likeCounter;
    }




}
