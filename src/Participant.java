/*26.06.2018, Nikola Bunis

A base class for participants, should be extended to sub-class authors.

Basic attribute - name string.

Basic Behaviour - like and download messages.

 */

public class Participant {

    private String name;

    public Participant(String name) {
        setName(name);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void likeMessage(){

    }



}
