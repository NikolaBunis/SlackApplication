/*26.06.2018, Nikola Bunis

Sub-class of the Message class.

Will extend it with having a string field for text and a like counter for the like functionality planned in this project.

 */

import java.util.HashSet;

public class TextMessage
        extends Message
        implements Likable
{


    private String text;

    private HashSet<String> likeCounter;


    public TextMessage(int timestamp, String author) {
        super(timestamp, author);
    }

    public TextMessage(int timestamp, String author, String text) {
        super(timestamp, author);
        setText(text);
    }



    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }


    public HashSet<String> getLikeCounter() {
        return likeCounter;
    }

    public void setLikeCounter(HashSet<String> likeCounter) {
        this.likeCounter.addAll(likeCounter);
    }

    public void addLike(Participant participant){

        getLikeCounter().add(participant.getName());

    }


    @Override
    public void like(Participant participant, Message message){

        addLike(participant);

        System.out.println(participant.getName() + " liked a message posted on " + message.getTimestamp());


    }



}
