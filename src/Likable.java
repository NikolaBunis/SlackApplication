/*26.06.2018, Nikola Bunis,

Interface depicting the "like" functionality for the project.

The like() method will be shared by the TextMessage and ImageMessage classes as per the project guidelines.

 */

public interface Likable {


    public void like(Participant participant, Message message);


}
