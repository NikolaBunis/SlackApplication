/*26.06.2018, Nikola Bunis

Sub-class of Message. Has a field fileName as a placeholder for an actual file.

Should have the download functionality.

 */

public class FileMessage extends Message {


    private String fileName;

    public FileMessage(int timestamp, String author) {
        super(timestamp, author);
    }

    public FileMessage(int timestamp, String author, String fileName) {
        super(timestamp, author);
        setFileName(fileName);
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }
}
